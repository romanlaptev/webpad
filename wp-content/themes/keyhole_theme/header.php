<?php
/**
  * @package WordPress
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
<!--
	<title>test theme1</title>
-->
	<?php wp_head(); ?>
</head>

<body <?php body_class();?>>

	<div class="header container">
		<div class="wrapper">
			<h1>website</h1>
		</div>

		<div class="menu container">
			<div class="wrapper">
<?php
 	wp_nav_menu( array( 
// 		'theme_location' => 'top_menu',
 		'menu' => 'menu1'
 	));
?>
			</div>
		</div><!-- end menu -->
		
		
	</div><!-- end header -->
