<?php
//add_filter( 'init', 'wp_init' );
add_action( 'after_setup_theme', 'keyhole_after_setup' );
//add_action( 'wp_footer', 'keyhole_footer' );

$hookName = "wp_enqueue_scripts";
$actionName = "keyhole_reg_scripts";
add_action( $hookName, $actionName );


function wp_init(){
	//echo "WP, plugins, theme loaded...";
	//echo "INIT functions, wp-content/themes/theme1/functions.php";
	//echo "<br>";
}//end

function keyhole_after_setup(){

	//add_theme_support("menus");
	register_nav_menus( [
		'top_menu' => 'header menu',
		'bottom_menu' => 'footer menu'
	] );	
}//end


function keyhole_footer(){
}//end

function keyhole_reg_scripts(){

	$styleName = "base-style";
	$styleLocation = get_stylesheet_uri();//style.css
	wp_enqueue_style( $styleName, $styleLocation);

	//$handle = "script1";
	//$src = get_template_directory_uri() . "/js/script1.js";
	//wp_enqueue_script( $handle, $src);

}//end
