<?php
/**
 * The main template file
 */
 
get_header();
//get_header("custom");// no wp_head();
?>

	<div class="main container">

		<div class="sidebar sidebar-left">
		  <div class="wrapper">
			<div class="panel"></div>
		  </div>
		
		  <div class="wrapper">
			<div class="panel"></div>
		  </div>
	  
		</div><!-- end sidebar -->

		<div class="content">
		  <div class="wrapper">
				<div class="panel">
<?php
//wp_page_menu();
wp_list_pages();
?>					
				</div>
			</div>
		</div><!-- end content -->
	
		
		<div class="sidebar sidebar-right">
		  <div class="wrapper">
				<div class="panel"></div>
		  </div>
		</div><!-- end sidebar -->

	</div><!-- end main -->


<?php
get_footer();
