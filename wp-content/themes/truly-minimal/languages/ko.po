# Translation of Truly Minimal in Korean
# This file is distributed under the same license as the Truly Minimal package.
msgid ""
msgstr ""
"PO-Revision-Date: 2014-06-02 04:44:52+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Truly Minimal\n"

#: sidebar.php:24
msgid "Meta"
msgstr "그 밖의 기능"

#: searchform.php:11
msgctxt "submit button"
msgid "Search"
msgstr "검색"

#: searchform.php:10
msgctxt "placeholder"
msgid "Search &hellip;"
msgstr "검색 &hellip;"

#: search.php:16
msgid "Search Results for: %s"
msgstr "검색 결과: %s"

#: no-results.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "찾고 계신 것을 찾을 수 없습니다. 검색이 도움이 될 수 있습니다."

#: no-results.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "죄송합니다. 검색어와 일치하는 결과가 없습니다. 다른 키워드로 다시 시도해보세요."

#: no-results.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "첫 번째 글을 게시할 준비가 되셨나요? <a href=\"%1$s\">여기서 시작하세요</a>."

#: no-results.php:13
msgid "Nothing Found"
msgstr "찾지 못함"

#: inc/template-tags.php:120
msgid "View all posts by %s"
msgstr "%s의 모든 글 보기"

#: inc/template-tags.php:84
msgid "Your comment is awaiting moderation."
msgstr "입력한 댓글이 검토를 기다리고 있습니다."

#: inc/template-tags.php:81
msgid "%s <span class=\"says\">says:</span>"
msgstr "%s의 <span class=\"says\">댓글:</span>"

#: inc/template-tags.php:71
msgid "Pingback:"
msgstr "핑백:"

#: inc/template-tags.php:48
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr "최신 글 <span class=\"meta-nav\">&rarr;</span>"

#: inc/template-tags.php:44
msgid "<span class=\"meta-nav\">&larr;</span> Older posts"
msgstr "<span class=\"meta-nav\">&larr;</span> 예전 글"

#: inc/template-tags.php:39
msgctxt "Next post link"
msgid "&rarr;"
msgstr "&rarr;"

#: inc/template-tags.php:38
msgctxt "Previous post link"
msgid "&larr;"
msgstr "&larr;"

#: inc/template-tags.php:34
msgid "Post navigation"
msgstr "글 네비게이션"

#: inc/extras.php:75
msgid "Page %s"
msgstr "페이지 %s"

#: inc/customizer.php:33
msgid "Left"
msgstr "왼쪽"

#: inc/customizer.php:32
msgid "Right"
msgstr "오른쪽"

#: inc/customizer.php:19
msgid "Theme Options"
msgstr "테마 옵션"

#: image.php:105
msgid "Both comments and trackbacks are currently closed."
msgstr "현재 댓글 및 트랙백이 모두 닫혀있습니다."

#: image.php:103
msgid "Trackbacks are closed, but you can <a class=\"comment-link\" href=\"#respond\" title=\"Post a comment\">post a comment</a>."
msgstr "트랙백이 닫혀 있습니다. 하지만 <a class=\"comment-link\" href=\"#respond\" title=\"댓글 달기\">댓글을 달 수 있습니다</a>."

#: image.php:101
msgid "Comments are closed, but you can leave a trackback: <a class=\"trackback-link\" href=\"%s\" title=\"Trackback URL for your post\" rel=\"trackback\">Trackback URL</a>."
msgstr "댓글이 닫혀 있지만 트랙백을 남길 수 있습니다: <a class=\"trackback-link\" href=\"%s\" title=\"글의 트랙백 URL\" rel=\"trackback\">트랙백 URL</a>."

#: image.php:99
msgid "<a class=\"comment-link\" href=\"#respond\" title=\"Post a comment\">Post a comment</a> or leave a trackback: <a class=\"trackback-link\" href=\"%s\" title=\"Trackback URL for your post\" rel=\"trackback\">Trackback URL</a>."
msgstr "<a class=\"comment-link\" href=\"#respond\" title=\"댓글 달기\">댓글을 달거나</a> 트랙백을 남기세요: <a class=\"trackback-link\" href=\"%s\" title=\"글의 트랙백 URL\" rel=\"trackback\">트랙백 URL</a>."

#: image.php:39
msgid "Next <span class=\"meta-nav\">&rarr;</span>"
msgstr "다음 <span class=\"meta-nav\">&rarr;</span>"

#: image.php:38
msgid "<span class=\"meta-nav\">&larr;</span> Previous"
msgstr "<span class=\"meta-nav\">&larr;</span> 이전"

#: header.php:41
msgid "Skip to content"
msgstr "컨텐츠로 건너뛰기"

#: header.php:40
msgid "Menu"
msgstr "메뉴"

#: functions.php:82
msgid "Sidebar"
msgstr "사이드바"

#: functions.php:53
msgid "Primary Menu"
msgstr "기본 메뉴"

#: footer.php:18
msgid "Theme: %1$s by %2$s."
msgstr "테마: %1$s(%2$s 제작)."

#: footer.php:16
msgid "Proudly powered by %s"
msgstr "Proudly powered by %s"

#: footer.php:16
msgid "A Semantic Personal Publishing Platform"
msgstr "의미론적 개인 출판 플랫폼"

#: content.php:37
msgid "Posted in %1$s"
msgstr "%1$s에 게시됨"

#: content-single.php:42
msgid "This entry was posted in %1$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "이 글은 %1$s 카테고리에 분류되었습니다. <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">고유주소</a> 북마크."

#: content-single.php:40
msgid "This entry was posted in %1$s and tagged %2$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "이 글은 %1$s 카테고리에 분류되었고 %2$s 태그가 있습니다. <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">고유주소</a> 북마크."

#: content-single.php:34
msgid "Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "<a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">고유주소</a>를 북마크하세요."

#: content-single.php:32
msgid "This entry was tagged %2$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "이 엔트리는 %2$s 태그가 지정되었습니다. <a href=\"%3$s\" title=\"%4$s의 퍼머링크\" rel=\"bookmark\">퍼머링크</a>를 북마크하세요."

#: content-image.php:47 content-video.php:47 content.php:55
msgid "% Comments"
msgstr "댓글 %개"

#: content-image.php:47 content-video.php:47 content.php:55
msgid "Leave a comment"
msgstr "댓글 남기기"

#: content-image.php:41 content-video.php:41 content.php:48
msgid "Tagged %1$s"
msgstr "%1$s 태그 지정"

#: content-image.php:30 content-video.php:30
msgid "in %1$s"
msgstr "카테고리: %1$s"

#. translators: used between list items, there is a space after the comma
#: content-image.php:26 content-image.php:36 content-single.php:24
#: content-single.php:27 content-video.php:26 content-video.php:36
#: content.php:33 content.php:43
msgid ", "
msgstr ", "

#: content-image.php:16 content-video.php:16 content.php:24
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr "계속 읽기 <span class=\"meta-nav\">&rarr;</span>"

#: content-aside.php:24 content-image.php:50 content-link.php:23
#: content-page.php:18 content-quote.php:24 content-single.php:56
#: content-video.php:50 content.php:58 image.php:34 image.php:107
#: inc/template-tags.php:71 inc/template-tags.php:92
msgid "Edit"
msgstr "편집"

#: content-aside.php:11 content-image.php:17 content-link.php:11
#: content-page.php:16 content-quote.php:11 content-single.php:18
#: content-video.php:17 content.php:25 image.php:93
msgid "Pages:"
msgstr "페이지:"

#: comments.php:70
msgid "Comments are closed."
msgstr "댓글은 닫혔습니다."

#: comments.php:40 comments.php:60
msgid "Newer Comments &rarr;"
msgstr "새로운 댓글 &rarr;"

#: comments.php:39 comments.php:59
msgid "&larr; Older Comments"
msgstr "&larr; 예전 댓글"

#: comments.php:38 comments.php:58
msgid "Comment navigation"
msgstr "댓글 네비게이션"

#: comments.php:31
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "&ldquo;%2$s&rdquo;에 대한 %1$s개의 생각"

#: archive.php:63 sidebar.php:17
msgid "Archives"
msgstr "글목록"

#: archive.php:60
msgid "Links"
msgstr "링크"

#: archive.php:57
msgid "Quotes"
msgstr "인용"

#: archive.php:54
msgid "Videos"
msgstr "비디오"

#: archive.php:51
msgid "Images"
msgstr "이미지"

#: archive.php:48
msgid "Asides"
msgstr "추가정보"

#: archive.php:45
msgid "Yearly Archives: %s"
msgstr "연간 보관물: %s"

#: archive.php:42
msgid "Monthly Archives: %s"
msgstr "월간 보관물: %s"

#: archive.php:39
msgid "Daily Archives: %s"
msgstr "일간 보관물: %s"

#: archive.php:31
msgid "Author Archives: %s"
msgstr "글쓴이 보관물: %s"

#: archive.php:24
msgid "Tag Archives: %s"
msgstr "태그 보관물: %s"

#: archive.php:21
msgid "Category Archives: %s"
msgstr "카테고리 보관물: %s"

#. translators: %1$s: smiley
#: 404.php:36
msgid "Try looking in the monthly archives. %1$s"
msgstr "월간 보관물을 확인해보세요. %1$s"

#: 404.php:27
msgid "Most Used Categories"
msgstr "가장 많이 사용된 카테고리"

#: 404.php:19
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr "이 위치에서 아무 것도 찾을 수 없는 것 같습니다. 아래의 링크 중 하나로 시도하거나 검색을 해보시는 것은 어떠세요?"

#: 404.php:15
msgid "Oops! That page can&rsquo;t be found."
msgstr "페이지를 찾을 수 없습니다."
