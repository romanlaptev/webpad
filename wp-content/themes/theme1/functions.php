<?php
//add_filter( 'init', 'wp_init' );
add_action( 'after_setup_theme', 'theme1_after_setup' );
add_action( 'wp_footer', 'theme1_footer' );

$hookName = "wp_enqueue_scripts";
$actionName = "theme1_reg_scripts";
add_action( $hookName, $actionName );


function wp_init(){
	echo "WP, plugins, theme loaded...";
	echo "INIT functions, wp-content/themes/theme1/functions.php";
	echo "<br>";
}//end

function theme1_after_setup(){
	echo "function  theme1_after_setup()";
	echo "<br>";

	//add_theme_support("menus");
	
	register_nav_menus( [
		'top_menu' => 'header menu',
		'bottom_menu' => 'footer menu'
	] );	
 	
 	//register_nav_menu( 'primary', __( 'Primary Menu' ) );
// 	register_nav_menu( 'primary', __( 'Primary Menu', 'twentyeleven' ) );

// 		register_nav_menus(
// 			array(
// 				'menu-1' => __( 'Primary', 'twentynineteen' ),
// 				'footer' => __( 'Footer Menu', 'twentynineteen' ),
// 				'social' => __( 'Social Links Menu', 'twentynineteen' ),
// 			)
// 		);

	add_theme_support("post-formats", array(
		"aside",
		"image",
		"video",
		"gallery"
	));
	
	
}//end


function theme1_footer(){
	echo "-- event 'wp_footer' ";
//	echo "function  theme1_footer()";
	echo "<br>";
	do_action("my_action");
}//end

function theme1_reg_scripts(){

//https://wp-kama.ru/function/wp_enqueue_style
	$styleName = "base-style";
	$styleLocation = get_stylesheet_uri();//style.css
	wp_enqueue_style( $styleName, $styleLocation);

echo get_stylesheet_uri();
echo "<br>";
echo get_template_directory_uri();
echo "<br>";

//https://wp-kama.ru/function/wp_register_script	
	$handle = "script1";
	$src = get_template_directory_uri() . "/js/script1.js";
	wp_enqueue_script( $handle, $src);

}//end


function myAction(){
	echo "-- event 'my_action' ";
	echo "<br>";
}//end
add_action("my_action", "myAction");
