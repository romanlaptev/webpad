<?php
/*
Plugin Name: Test DB
Author: romanlaptev
Version: 1.0
*/
require_once( __DIR__.'/../utils.php' );

register_activation_hook( 
	__FILE__, //filepath
	'wfm_activate'//callback 
);

register_deactivation_hook( __FILE__, 'wfm_deactivate');
//register_uninstall_hook( __FILE__, 'wfm_unistall');

//----------------------------------
function wfm_activate(){
//https://wp-kama.ru/function/wpdb
	global $wpdb;
	
	//$query = "SELECT * FROM ".$wpdb->prefix."options";

	//$result = $wpdb->get_results( $query );
//echo _logWrap($result);

//foreach ( $fivesdrafts as $fivesdraft ) {
	//echo $fivesdraft->post_title;
//}

	$tableName = $wpdb->prefix . 'test_table';
	$query = "CREATE TABLE IF NOT EXISTS ".$tableName." (
id int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255) DEFAULT NULL,
price DECIMAL(9,2) NOT NULL DEFAULT '0.00'
) DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;";
	$wpdb->query($query);

//https://oiplug.com/blog/wordpress/4143/	
 //require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
 //dbDelta( $query );
 
}//end

function wfm_deactivate(){
	global $wpdb;

	$tableName = $wpdb->prefix.'test_table';
	$query = "DROP TABLE IF EXISTS ".$tableName;
	$wpdb->query($query);	
}//end


?>
