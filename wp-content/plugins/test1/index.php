<?php
/**
 * @package test1
 * @version 1.0
 */
/*
Plugin Name: Test 1
Description: TEST.
Author: romanlaptev
Version: 1.0
*/

register_activation_hook( 
	__FILE__, //filepath
	'wfm_activate'//callback 
);
register_activation_hook( __FILE__, 'wfm_activate2');
//register_deactivation_hook( __FILE__, 'wfm_deactivate');
//register_uninstall_hook( __FILE__, 'wfm_unistall');

function wfm_activate(){
	$d = date("Y-m-d H:i:s");
	error_log( 
		$d." - used hook 'register_activation_hook', function wfm_activate()\n", 
		3,
		dirname(__FILE__)."/log.log"
	);
	//wp_mail( get_bloginfo('admin_email'),  'test wp plugin', 'Plugin has been activated...');
}//end

function wfm_activate2(){
	$d = date("Y-m-d H:i:s");
	error_log( $d." - next call function wfm_activate2()\n", 3,	dirname(__FILE__)."/log.log");
}//end


//------------------------ TEST FILTERS
//https://codex.wordpress.org/Plugin_API/
//https://adambrown.info/p/wp_hooks

$hookName = "the_title";
$filterName = "wfm_title";
add_filter( $hookName, $filterName);

function wfm_title( $title ){
	if( is_admin() ){
		return $title;
	}
	return mb_convert_case( $title, MB_CASE_UPPER, "UTF-8");
}//end


$hookName = "the_content";
$filterName = "wfm_content";
add_filter( $hookName, $filterName);

function wfm_content( $content ){
	
	if( is_page() ){
		return $content;
	}
	
	if( is_user_logged_in() ){
		return $content;
	}
	
	$link = "<a href='". home_url()."/wp-login.php'>wp login</a>";
	return "access denied...you must be logged in ".$link;
}//end


//------------------------ TEST ACTIONs
$hookName = "comment_post";
$actionName = "wfm_comment";
add_filter( $hookName, $actionName);

function wfm_comment( $content ){
	$d = date("Y-m-d H:i:s");
	error_log( $d." - add new comment\n", 3,	dirname(__FILE__)."/log.log");
}//end


?>
